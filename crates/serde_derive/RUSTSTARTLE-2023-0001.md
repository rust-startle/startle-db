```toml
[advisory]
id = "RUSTSTARTLE-2023-0001"
package = "serde_derive"
date = "2023-08-03"
url = "https://github.com/serde-rs/serde/issues/2538"
informational = "notice"
categories = []

[affected]
arch = ["x86_64"]

[versions]
patched = []
unaffected = ["< 1.0.172"]
```

# Executable in packaged crate

Affected versions of this crate distribute a pre-compiled executable. This
executable, `serde_derive-x86_64-unknown-linux-gnu`, is used to
automatically derive implementations of the `Serialize` and `Deserialize`
traits on `x86_64`. There is no fallback to using the compiled source that is
used on other architectures.

This may be a problem for downstream packagers, who may be subject to special
rules regarding executables from upstream in packages.
